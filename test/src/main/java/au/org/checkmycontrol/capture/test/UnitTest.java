package au.org.checkmycontrol.capture.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;

import au.org.checkmycontrol.capture.LoginActivity_;
import au.org.checkmycontrol.capture.R;

import static org.junit.Assert.assertEquals;


@RunWith(RobolectricGradleTestRunner.class)
public class UnitTest {
    @Test
    public void clickingLogin_shouldStartLoginActivity() {
        LoginActivity_ activity = Robolectric.setupActivity(LoginActivity_.class);
        android.widget.Button btn = (android.widget.Button) activity.findViewById(R.id.btn_log_in);
        assertEquals(btn.getText(), "Confirm");
    }
}
