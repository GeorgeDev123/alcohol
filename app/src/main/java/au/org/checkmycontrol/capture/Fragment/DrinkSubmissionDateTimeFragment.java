package au.org.checkmycontrol.capture.Fragment;

import android.preference.PreferenceFragment;
import android.support.v7.app.AppCompatActivity;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.PreferenceByKey;
import org.androidannotations.annotations.PreferenceClick;
import org.androidannotations.annotations.PreferenceScreen;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import au.org.checkmycontrol.capture.Preference.DateTimePreference;
import au.org.checkmycontrol.capture.R;

/**
 * Created by zhi on 4/10/2015.
 */
@PreferenceScreen(R.xml.pref_datetime_submission)
@EFragment
public class DrinkSubmissionDateTimeFragment extends PreferenceFragment {
    @PreferenceByKey(R.string.start_date)
    DateTimePreference preferenceStartDatetime;

    @PreferenceByKey(R.string.end_date)
    DateTimePreference preferenceEndDatetime;

    @PreferenceClick(R.string.start_date)
         void startDateTimePreferenceOnClick() {
        SlideDateTimeListener listener = setListener(preferenceStartDatetime);
        new SlideDateTimePicker.Builder(((AppCompatActivity)getActivity()).getSupportFragmentManager())
                .setListener(listener)
                .setInitialDate(new Date())
//                        .setMinDate(minDateTime)
//                        .setMaxDate(maxDateTime)
                        //.setIs24HourTime(true)
                        //.setTheme(SlideDateTimePicker.HOLO_DARK)
                        //.setIndicatorColor(Color.parseColor("#990000"))
                .build()
                .show();
    }

    @PreferenceClick(R.string.end_date)
    void endDateTimePreferenceOnClick() {
        SlideDateTimeListener listener = setListener(preferenceStartDatetime);
        new SlideDateTimePicker.Builder(((AppCompatActivity)getActivity()).getSupportFragmentManager())
                .setListener(listener)
                .setInitialDate(new Date())
//                        .setMinDate(minDateTime)
//                        .setMaxDate(maxDateTime)
                        //.setIs24HourTime(true)
                        //.setTheme(SlideDateTimePicker.HOLO_DARK)
                        //.setIndicatorColor(Color.parseColor("#990000"))
                .build()
                .show();
    }

    private SlideDateTimeListener setListener(final DateTimePreference preferenceDatetime){

        return new SlideDateTimeListener() {

            @Override
            public void onDateTimeSet(Date date)
            {
                SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.getDefault());
                preferenceDatetime.setSummary(simpleDateFormat.format(date));
            }
        };

    }
}
