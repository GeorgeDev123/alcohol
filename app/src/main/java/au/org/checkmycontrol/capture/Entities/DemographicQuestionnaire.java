package au.org.checkmycontrol.capture.Entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * Created by zhi on 12/10/2015.
 */
@Data
public class DemographicQuestionnaire {
    @JsonProperty("id")
    Long id;
    @JsonProperty("gender")
    private String gender;
    @JsonProperty("heightInCm")
    private Double heightInCm;
    @JsonProperty("weightInKg")
    private Double weightInKg;
    @JsonProperty("country")
    private String country;
}
