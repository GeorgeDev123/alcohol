package au.org.checkmycontrol.capture;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

import au.org.checkmycontrol.capture.Adapter.AccountAdapter;
import au.org.checkmycontrol.capture.Entities.AppData;
import au.org.checkmycontrol.capture.Entities.CapturePointOne;
import au.org.checkmycontrol.capture.Entities.DemographicQuestionnaire;
import au.org.checkmycontrol.capture.Entities.Participant;
import au.org.checkmycontrol.capture.Model.ItemPersonalDetails;
import au.org.checkmycontrol.capture.Services.ApiServices;
import au.org.checkmycontrol.capture.Services.ParticipantServices;
import au.org.checkmycontrol.capture.Services.UserPrefs_;
import au.org.checkmycontrol.capture.utils.Utils;

@EActivity(R.layout.activity_account)
public class AccountActivity extends AppCompatActivity {
    @Bean
    ApiServices apiService;
    @Bean
    ParticipantServices participantServices;
    @Pref
    UserPrefs_ userPrefs;

    @ViewById(R.id.toolbar)
    Toolbar mToolbar;
    @ViewById(R.id.tv_account_list_placeholder)
    TextView tvListPlaceHolder;
    @ViewById(R.id.list_account_details)
    ListView listView;

    private ArrayList<ItemPersonalDetails> personalDetailsList;

    @AfterViews
    void setUp(){
        if(userPrefs.appId().exists() && !StringUtils.isBlank(userPrefs.appId().get())) {
            try {
                Participant participant = participantServices.getParticipant();
                SetUpAccountDetails(participant);
                //getAccount();
            } catch (Exception e) {
                onError("Error on local data try to retrieve the participant details");
                getAccount();
            }
        }
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @UiThread
    void SetUpAccountDetails(Participant participant) {
        if(participant == null || participant.getId() == null) return;
        personalDetailsList = new ArrayList<ItemPersonalDetails>();
        DemographicQuestionnaire demographicQuestionnaire = null;
        CapturePointOne capturePointOne = participant.getCapturePointOne();
        if(capturePointOne != null){
            demographicQuestionnaire = capturePointOne.getDemographicQuestionnaire();
        }
        ItemPersonalDetails itemParticipantNumber = new ItemPersonalDetails(getString(R.string.participant_number), participant.getId());
        ItemPersonalDetails itemCountry = new ItemPersonalDetails(getString(R.string.country), demographicQuestionnaire == null? "" : demographicQuestionnaire.getCountry());
        ItemPersonalDetails itemGender = new ItemPersonalDetails(getString(R.string.gender),  demographicQuestionnaire  == null? "" : demographicQuestionnaire.getGender());
        ItemPersonalDetails itemDOB = new ItemPersonalDetails(getString(R.string.date_of_birth), participant.getDateOfBirth().toString());
        ItemPersonalDetails itemWeight = new ItemPersonalDetails(getString(R.string.weight), demographicQuestionnaire == null? "" : demographicQuestionnaire.getWeightInKg().toString());
        ItemPersonalDetails itemHeight = new ItemPersonalDetails(getString(R.string.height), demographicQuestionnaire == null? "" : demographicQuestionnaire.getHeightInCm().toString());
        AppData appData = null;
        if(capturePointOne != null) {
            appData = capturePointOne.getAppData();
        }
        ItemPersonalDetails itemDeactivatedDate = new ItemPersonalDetails(getString(R.string.deactivated_date), appData == null? "" : appData.getDeactivationDate().toString());
        //userPrefs.participantNumber().put(jsonObject.getJSONObject("_links").getJSONObject("self").getString("href"));

        personalDetailsList.add(itemParticipantNumber);
        personalDetailsList.add(itemCountry);
        personalDetailsList.add(itemGender);
        personalDetailsList.add(itemDOB);
        personalDetailsList.add(itemWeight);
        personalDetailsList.add(itemHeight);
        personalDetailsList.add(itemDeactivatedDate);

        AccountAdapter accountAdapter = new AccountAdapter(this, personalDetailsList);
        listView.setEmptyView(tvListPlaceHolder);
        listView.setAdapter(accountAdapter);
    }

    @Background
    void getAccount() {
        try {
            Participant participant = apiService.validAppId(participantServices.getAppId());
            SetUpAccountDetails(participant);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @UiThread
    void onError(String message) {
        Utils.setAlertDialog(getString(R.string.warning), message, this).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_account, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        super.onOptionsItemSelected(item);
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            Intent settingIntent = new Intent(this, SettingActivity_.class);
//            startActivity(settingIntent);
//            return true;
//        }
        if (id == android.R.id.home) {
            Intent homeIntent = new Intent(this,NavigationActivity_.class);
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            setResultIntent(homeIntent);
            this.startActivityForResult(homeIntent, 1);
            finish();
            return true;
        }
        return false;
    }

    @Override
    public Intent getParentActivityIntent () {
        Intent intent = super.getParentActivityIntent();
        setResultIntent(intent);
        return intent;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResultIntent(null);
        finish();
    }

    private void setResultIntent(Intent intent){
        if(intent == null) {
            intent = new Intent();
        }
        intent.putExtra("activity", true);
        setResult(RESULT_OK, intent);
    }
}
