package au.org.checkmycontrol.capture;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Calendar;

import au.org.checkmycontrol.capture.Adapter.HistoryAdapter;
import au.org.checkmycontrol.capture.DBA.HistoryDataSource;
import au.org.checkmycontrol.capture.Model.SubmitionHistory;


@EActivity(R.layout.activity_history)

public class HistoryActivity extends AppCompatActivity {
    @ViewById(R.id.lv_history)
    ListView history;

    ArrayList<SubmitionHistory> listHistory;

    @Bean
    HistoryDataSource dataSource;

    @ViewById(R.id.toolbar) Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    void setUp(){
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR, -24);
        dataSource.open();
        listHistory = dataSource.getLast24HourHistory(calendar.getTime());
        HistoryAdapter historyAdapter = new HistoryAdapter(this, listHistory);
        history.setAdapter(historyAdapter);
        dataSource.close();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_history, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResultIntent(null);
    }

    public Intent getParentActivityIntent () {
        Intent intent = super.getParentActivityIntent();
        setResultIntent(intent);
        return intent;
    }

    private void setResultIntent(Intent intent){
        if(intent == null) {
            intent = new Intent();
        }
        intent.putExtra("activity", true);
        setResult(RESULT_OK, intent);
        finish();
    }
}
