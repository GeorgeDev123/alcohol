package au.org.checkmycontrol.capture.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import org.threeten.bp.Clock;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZonedDateTime;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by zhi on 30/07/2015.
 */
public class Utils {
    public static boolean IsNullOrWhiteSpace(String string){
        if(string != null && string.trim().length() > 0){
            return false;
        }
        return true;
    }

    public static String showTime (int hour, int min){
        String format = "";
        if (hour == 0) {
            hour += 12;
            format = "AM";
        }
        else if (hour == 12) {
            format = "PM";
        } else if (hour > 12) {
            hour -= 12;
            format = "PM";
        } else {
            format = "AM";
        }
        return new StringBuilder().append(String.format("%02d", hour)).append(" : ").append(String.format("%02d", min))
                .append(" ").append(format).toString();
    }

    public static boolean isNetworkConnected(Context mContext){
        ConnectivityManager connMgr = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    public static AlertDialog.Builder setAlertDialog(String title, String message, Context context) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false);
        return alertDialogBuilder;
    }

    public static String showOnlyDate(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }

    public static String showOnlyTime(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return  sdf.format(date);
    }

    public static String convertTime(String _24HourFormat){
        String[] time = _24HourFormat.split(":");
        int hour = Integer.valueOf(time[0]);
        int minute = Integer.valueOf(time[1]);

        if(hour > 12){
            Integer ampmValue = hour - 12;
            return String.format("%02d",ampmValue) + " : " + String.format("%02d",minute) + " PM";
        }
        else if(hour == 12){
            return String.format("%02d",hour) + " : " + String.format("%02d", minute) + " PM";
        }
        return String.format("%02d",hour) + " : " + String.format("%02d", minute) + " AM";
    }

    public static boolean isApi19(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            return true;
        }
        return false;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
    public static Calendar getCalendar(){
        return Calendar.getInstance();
    }
    public static Clock getClock()
    {return Clock.systemDefaultZone();}

    public static ZonedDateTime convertDate(Date dateLegacy){
        if(dateLegacy == null) return null;
        Calendar calendar = getCalendar();
        calendar.setTime(dateLegacy);
        LocalDateTime ldt = LocalDateTime.of(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DATE), calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND), calendar.get(Calendar.MILLISECOND));
        return ldt.atZone(ZoneId.systemDefault());
    }
}
