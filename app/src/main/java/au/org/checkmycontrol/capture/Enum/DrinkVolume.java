package au.org.checkmycontrol.capture.Enum;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Created by zhi on 5/10/2015.
 */
@RequiredArgsConstructor
public enum DrinkVolume {
    ML_30(0.030d),
    ML_60(0.060d),
    ML_90(0.090d),
    ML_120(0.120d),
    ML_150(0.150d),
    ML_200(0.200d),
    ML_285(0.285d),
    ML_300(0.300d),
    ML_330(0.330d),
    ML_375(0.375d),
    ML_425(0.425d),
    ML_500(0.500d),
    ML_570(0.570d);
    @Getter
    private final double litres;
}
