package au.org.checkmycontrol.capture.Services;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.util.Calendar;

import au.org.checkmycontrol.capture.utils.Utils;

/**
 * Created by zhizheng on 24/08/2015.
 */

@EBean
public class NotificationUtils {
    public final int morningTimeId = 1;
    public final int eveningTimeId = 2;

    @Pref
    UserPrefs_ userPrefs;

    public void  setNotification(Calendar calendar, Context context, int alarmId) {
        Intent myIntent = new Intent(context, NotificationReceiver.class);
        myIntent.setAction("android.app.PendingIntent");
        myIntent.putExtra(NotificationServices.NOTIFY_ID, alarmId);
        myIntent.putExtra(NotificationServices.NOTIFY_HOUR, calendar.get(Calendar.HOUR_OF_DAY));
        myIntent.putExtra(NotificationServices.NOTIFY_Minutes, calendar.get(Calendar.MINUTE));
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
        if (PendingIntent.getBroadcast(context, alarmId, myIntent, PendingIntent.FLAG_NO_CREATE) != null) {
            PendingIntent oldIntent = PendingIntent.getBroadcast(context, alarmId, myIntent, PendingIntent.FLAG_NO_CREATE);
            alarmManager.cancel(oldIntent);
        }
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, alarmId, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        if (calendar.getTime().before(Calendar.getInstance().getTime())) {
            calendar.add(Calendar.HOUR, 24);
        }
        if (Utils.isApi19()) {
            setAlarmKitKat(calendar, alarmManager, pendingIntent);
        } else {
            setAlarm(calendar, alarmManager, pendingIntent);
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void setAlarmKitKat(Calendar calendar, AlarmManager alarmManager, PendingIntent pendingIntent) {
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
    }

    private void setAlarm(Calendar calendar, AlarmManager alarmManager, PendingIntent pendingIntent) {
        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
    }

    public Calendar getMorningNotificationCalendar() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 8);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        return calendar;
    }

    public Calendar getEveningNotificationCalendar() {
        String[] time = userPrefs.eveningNotification().get().split(":");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(time[0]));
        calendar.set(Calendar.MINUTE, Integer.valueOf(time[1]));
        calendar.set(Calendar.SECOND, 0);

        return calendar;
    }

    public void CancelAllAlarms(Context context){
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
        Intent myIntent = new Intent(context, NotificationReceiver.class);
        myIntent.setAction("android.app.PendingIntent");
        PendingIntent pendingMorningIntent = PendingIntent.getBroadcast(context, morningTimeId, myIntent, PendingIntent.FLAG_NO_CREATE);
        if(pendingMorningIntent != null){
            alarmManager.cancel(pendingMorningIntent);
        }
        PendingIntent pendingEvenningIntent = PendingIntent.getBroadcast(context, eveningTimeId, myIntent, PendingIntent.FLAG_NO_CREATE);
        if(pendingEvenningIntent != null){
            alarmManager.cancel(pendingEvenningIntent);
        }
    }

}
