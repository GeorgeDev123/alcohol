package au.org.checkmycontrol.capture.utils;

import android.app.Application;
import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

import au.org.checkmycontrol.capture.Model.DrinkSize;
import au.org.checkmycontrol.capture.Model.DrinkType;
import au.org.checkmycontrol.capture.R;

/**
 * Created by zhi on 5/08/2015.
 */
public class DrinksTypesDataHolder extends Application {
    private ArrayList<DrinkType> data;
    public ArrayList<DrinkType> getData(){return data;}
    public void setData(ArrayList<DrinkType> data){this.data = data;}
    private static DrinksTypesDataHolder dataHolder;
    public DrinksTypesDataHolder(Context context){
        InputStream inputStream = context.getResources().openRawResource(R.raw.drinks);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        int ctr;
        try {
            ctr = inputStream.read();
            while (ctr != -1) {
                byteArrayOutputStream.write(ctr);
                ctr = inputStream.read();
            }
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            // Parse the data into jsonobject to get original data in form of json.
            JSONObject jObject = new JSONObject(
                    byteArrayOutputStream.toString());
            JSONArray jArray = jObject.getJSONArray("Drinks");
            ArrayList<DrinkType> data = new ArrayList<DrinkType>();
            for (int i = 0; i < jArray.length(); i++) {
                JSONObject subObj = jArray.getJSONObject(i);
                JSONObject sizes = subObj.getJSONObject("Sizes");
                Iterator<String> keys = sizes.keys();
                ArrayList<DrinkSize> drinkSizes = new ArrayList<DrinkSize>();
                while(keys.hasNext()){
                    String key = keys.next();
                    if(sizes.optJSONArray(key) != null){
                        JSONArray array = sizes.getJSONArray(key);
                        for(int count = 0; count < array.length(); count++){
                            drinkSizes.add(new DrinkSize(key, array.get(count).toString()));
                        }
                    }
                    else {
                        String value = sizes.optString(key);
                        DrinkSize drinkSize = new DrinkSize(key, value);
                        drinkSizes.add(drinkSize);
                    }
                }
                String imageName = subObj.getString("ImageName");
                Double percentage =  subObj.getDouble("AlcoholPercentage");
                String drinkTypeValue = subObj.getString("DrinkType");
                String submitEnum = subObj.getString("SubmitEnum");
                DrinkType drinkType = new DrinkType(imageName, percentage,drinkTypeValue,drinkSizes,submitEnum);
                data.add(drinkType);
            }
            setData(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static DrinksTypesDataHolder getInstance(Context c) {
        if(dataHolder == null){
            dataHolder = new DrinksTypesDataHolder(c);
        }
        return dataHolder;
    }

}
