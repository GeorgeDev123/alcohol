package au.org.checkmycontrol.capture.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import au.org.checkmycontrol.capture.R;

/**
 * Created by zhi on 5/08/2015.
 */
public class DrinkType implements Parcelable {
    private String ImageName;
    private double Percentage;
    private String DrinkType;
    private String SubmitEnum;
    private ArrayList<DrinkSize> Sizes = new ArrayList<DrinkSize>();

    public DrinkType(String ImageName, Double Percentage, String DrinkType, ArrayList<DrinkSize> Sizes, String SubmitEnum){
        this.ImageName = ImageName;
        this.Percentage = Percentage;
        this.DrinkType = DrinkType;
        this.Sizes = Sizes;
        this.SubmitEnum = SubmitEnum;
    }

    private DrinkType(Parcel in) {
        ImageName = in.readString();
        Percentage = in.readDouble();
        DrinkType = in.readString();
        SubmitEnum = in.readString();
        in.readTypedList(Sizes, DrinkSize.CREATOR);
    }

    public static final Creator<au.org.checkmycontrol.capture.Model.DrinkType> CREATOR = new Creator<au.org.checkmycontrol.capture.Model.DrinkType>() {
        @Override
        public au.org.checkmycontrol.capture.Model.DrinkType createFromParcel(Parcel in) {
            return new DrinkType(in);

        }

        @Override
        public au.org.checkmycontrol.capture.Model.DrinkType[] newArray(int size) {
            return new DrinkType[size];
        }
    };

    public String getImageName() {
        return ImageName;
    }

    public Integer getImageId(){
        String name = getImageName().split("\\.")[0];
        if(name.toLowerCase().equalsIgnoreCase("beer")){
            return R.drawable.beer;
        }
        else if(name.toLowerCase().equalsIgnoreCase("whitewine")){
            return R.drawable.whitewine;
        }
        else if(name.toLowerCase().equalsIgnoreCase("redwine")){
            return R.drawable.redwine;
        }
        else if(name.toLowerCase().equalsIgnoreCase("fortifiedwine")){
            return R.drawable.fortifiedwine;
        }
        else if(name.toLowerCase().equalsIgnoreCase("premixdrink")){
            return R.drawable.premixdrink;
        }
        else if(name.toLowerCase().equalsIgnoreCase("spiritliquerglass")){
            return R.drawable.spiritliquerglass;
        }
        else if(name.toLowerCase().equalsIgnoreCase("cocktail")){
            return R.drawable.cocktail;
        }
        else if(name.toLowerCase().equalsIgnoreCase("beer")){
            return R.drawable.beer;
        }
        return null;
    }

    public Double getPercentage() {
        return Percentage;
    }

    public String getDrinkType() {
        return DrinkType;
    }

    public String getSubmitEnum(){return SubmitEnum;}

    public ArrayList<DrinkSize> getSizes() {
        return Sizes;
    }

    public String[] getSizesArray(){
        ArrayList<String> sizeValues = new ArrayList<String>();
        for(DrinkSize size : getSizes()) {
            sizeValues.add(size.getDrinkSizeDisplay());
        }
        return sizeValues.toArray(new String[sizeValues.size()]);
    }

    public String[] getSizesValues(){
        ArrayList<String> sizeValues = new ArrayList<String>();
        for(DrinkSize size : getSizes()) {
            sizeValues.add(size.getDrinkSizeDisplay());
        }
        return sizeValues.toArray(new String[sizeValues.size()]);
    }

    public void setImageName(String imageName) {
        ImageName = imageName;
    }

    public void setPercentage(double percentage) {
        Percentage = percentage;
    }

    public void setDrinkType(String drinkType) {
        DrinkType = drinkType;
    }

    public void setSizes(ArrayList<DrinkSize> sizes){
        Sizes = sizes;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ImageName);
        dest.writeDouble(Percentage);
        dest.writeString(DrinkType);
        dest.writeString(SubmitEnum);
        dest.writeTypedList(Sizes);
    }
}
