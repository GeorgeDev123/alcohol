package au.org.checkmycontrol.capture.Fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;

import com.google.common.base.Functions;
import com.google.common.collect.ContiguousSet;
import com.google.common.collect.DiscreteDomain;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Range;
import com.google.common.primitives.Ints;

import org.androidannotations.annotations.AfterPreferences;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.PreferenceByKey;
import org.androidannotations.annotations.PreferenceChange;
import org.androidannotations.annotations.PreferenceClick;
import org.androidannotations.annotations.PreferenceScreen;

import java.util.List;

import au.org.checkmycontrol.capture.DrinkTypeActivity_;
import au.org.checkmycontrol.capture.Model.DrinkType;
import au.org.checkmycontrol.capture.Preference.NavigationPreference;
import au.org.checkmycontrol.capture.Preference.NumberPickerPreference;
import au.org.checkmycontrol.capture.R;
import au.org.checkmycontrol.capture.utils.Utils;
import lombok.Getter;


/**
 * Created by zhi on 31/08/2015.
 */

@PreferenceScreen(R.xml.pref_details)
@EFragment
public class DetailsFragment extends PreferenceFragment {
    @PreferenceByKey(R.string.pref_details_type)
    NavigationPreference preferenceType;

    @PreferenceByKey(R.string.pref_details_sizes)
    ListPreference preferenceSizes;

    @PreferenceByKey(R.string.pref_details_percentage)
    Preference preferenceAlcohol;

    @PreferenceByKey(R.string.pref_details_quantity)
    NumberPickerPreference preferenceQuantity;

    @PreferenceByKey(R.string.pref_title_drink_details)
    PreferenceCategory preferenceCategory;

    @Getter
    @FragmentArg
    DrinkType drinkType;

    @Getter
    int quantity = -1;

    @Getter
    int size = -1;

    @AfterPreferences
    void initPrefs() {
        if (drinkType != null) {
            preferenceType.setSummary(drinkType.getDrinkType());
            preferenceSizes.setEntries(drinkType.getSizesArray());
            int[] ints = Ints.toArray(ContiguousSet.create(Range.closed(0, drinkType.getSizes().size() - 1), DiscreteDomain.integers()));
            List<String> entryValues = Lists.newArrayList(Iterables.transform(Ints.asList(ints), Functions.toStringFunction()));
            preferenceSizes.setEntryValues(entryValues.toArray(new String[entryValues.size()]));
            preferenceSizes.setValue(null);
            preferenceQuantity.setValue(1);
            preferenceAlcohol.setSummary(drinkType.getPercentage().toString() + "%");
        } else {
            String unknown = getResources().getString(R.string.unknown);
            String no_drink = getResources().getString(R.string.no_drink);
            preferenceType.setTitle(getString(R.string.type));
            preferenceType.setSummary(unknown);
            preferenceSizes.setSummary(no_drink);
            preferenceSizes.setEnabled(false);
            preferenceAlcohol.setSummary(unknown);
            preferenceQuantity.setSummary(no_drink);
            preferenceQuantity.setEnabled(false);
        }
    }

    @PreferenceChange(R.string.pref_details_sizes)
    void sizePreferenceChange(String newValue){
        int index = Integer.parseInt(newValue);
        preferenceSizes.setSummary(drinkType.getSizes().get(Integer.parseInt(newValue)).getDrinkSizeDisplay());
        size = index;
    }

    @PreferenceChange(R.string.pref_details_quantity)
    void quantityPreferenceChange(Object newValue){
        preferenceQuantity.setSummary(((Integer) newValue).toString());
        quantity = (Integer) newValue;
    }

    @PreferenceClick(R.string.pref_details_sizes)
    void sizePrefereneClick() {
        if (drinkType == null) {
            Utils.setAlertDialog(getString(R.string.warning), "Error", this.getActivity()).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).show();
        }
    }

    @PreferenceClick(R.string.pref_details_type)
    void typePreferenceOnClick() {
        Intent typeIntent = new Intent(getActivity(), DrinkTypeActivity_.class);
        typeIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(typeIntent);
    }
}
