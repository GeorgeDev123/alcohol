package au.org.checkmycontrol.capture.ViewGroup;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import au.org.checkmycontrol.capture.Model.DrinkSummary;
import au.org.checkmycontrol.capture.R;

/**
 * Created by zhi on 5/10/2015.
 */
@EViewGroup(R.layout.item_drink_selection)
public class DrinkSubmissionItemView extends LinearLayout {
    @ViewById(R.id.tv_type_summary_name)
    TextView tvDrinkName;
    @ViewById(R.id.tv_type_summary_description)
    TextView tvDrinkDescription;

    private final String pipe = " | ";
    public DrinkSubmissionItemView(Context context){
        super(context);
    }
    public void bind(DrinkSummary drinkSummary) {
        tvDrinkName.setText(drinkSummary.getDrinkType().getDrinkType());
        tvDrinkDescription.setText(getResources().getString(R.string.size) + ": "
                + drinkSummary.getSize() + pipe + "Alcohol: " +drinkSummary.getDrinkType().getPercentage() + "%"
                + pipe + "x" + drinkSummary.getQuantity());
    }
}
