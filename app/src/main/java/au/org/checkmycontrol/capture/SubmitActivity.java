package au.org.checkmycontrol.capture;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.threeten.bp.ZonedDateTime;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import au.org.checkmycontrol.capture.DBA.HistoryDataSource;
import au.org.checkmycontrol.capture.Entities.Drink;
import au.org.checkmycontrol.capture.Exception.NetWorkException;
import au.org.checkmycontrol.capture.Exception.ServerValidationException;
import au.org.checkmycontrol.capture.Model.DrinkSummary;
import au.org.checkmycontrol.capture.Services.ApiServices;
import au.org.checkmycontrol.capture.Services.BOSubmit;
import au.org.checkmycontrol.capture.Services.DrinkServices;
import au.org.checkmycontrol.capture.Services.UserPrefs_;
import au.org.checkmycontrol.capture.utils.Utils;



@EActivity(R.layout.activity_submit)
public class SubmitActivity extends AppCompatActivity {
    @ViewById(R.id.listView) com.baoyz.swipemenulistview.SwipeMenuListView list;
    @ViewById(R.id.toolbar) Toolbar mToolbar;
    @Bean HistoryDataSource dataSource;

    @Pref
    UserPrefs_ userPrefs;

    public static ArrayList<DrinkSummary> selectedDrinks = new ArrayList<DrinkSummary>();
    private static Date startDateTime;
    private static Date endDateTime;
    private SimpleDateFormat simpleDateFormat=new SimpleDateFormat("EEE dd MMM hh:mm a",Locale.getDefault());
    private TextView startDateDisplay;
    private TextView endDateDisplay;
    private ProgressDialog pd;

    @Bean
    ApiServices apiService;
    @Bean
    BOSubmit boSubmit;
    @Bean
    DrinkAdapter drinkAdapter;
    @Bean
    DrinkServices drinkServce;

    @ViewById(R.id.submit_drink_consumed_title)
    TextView drinksTitle;
    @ViewById(R.id.add_new_drink_titile)
    TextView addDrinksTitle;
    @ViewById(R.id.divider_new_drink)
    View dividerAddDrink;

    @ViewById(R.id.add_new_drink)
    RelativeLayout addDrink;
    @Click(R.id.add_new_drink)
    void onClickAddNew() {
        Intent intent = new Intent(this, DrinkTypeActivity_.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }

    @ViewById(R.id.start_date)
    RelativeLayout layoutStartDate;
    @Click(R.id.start_date)
    void onClickStartDate(){
        startDateDisplay = (TextView) layoutStartDate.findViewById(R.id.cust_pref_summary);
        show(startDateDisplay, true);
    }

    @ViewById(R.id.end_date)
    RelativeLayout layoutEndDate;
    @Click(R.id.end_date)
    void onClickEndDate(){
        endDateDisplay = (TextView) layoutEndDate.findViewById(R.id.cust_pref_summary);
        show(endDateDisplay,false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent i = getIntent();
        if(getIntent().hasExtra("Drink")) {
            DrinkSummary drink = i.getParcelableExtra("Drink");
            selectedDrinks.add(drink);
        }
    }

    @AfterViews
    void setUp(){
        startDateDisplay = (TextView) layoutStartDate.findViewById(R.id.cust_pref_summary);
        endDateDisplay = (TextView) layoutEndDate.findViewById(R.id.cust_pref_summary);
        list.setAdapter(drinkAdapter);
        Utils.setListViewHeightBasedOnChildren(list);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_cancel_dark);
        createSwipteMenu();
        setIncludeViews();
        onToggleTiltes();
    }

    private void setIncludeViews() {
        ((TextView)addDrink.findViewById(R.id.cust_pref_title)).setText(getString(R.string.add_drink));
        ((TextView)addDrink.findViewById(R.id.cust_pref_summary)).setText(getString(R.string.tab_to_add));
        ((TextView)layoutStartDate.findViewById(R.id.cust_pref_title)).setText(getString(R.string.start_date));
        ((TextView)layoutEndDate.findViewById(R.id.cust_pref_title)).setText(getString(R.string.end_date));
    }

    void createSwipteMenu() {
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(dp2px(90));
                // set a icon
                deleteItem.setIcon(R.drawable.ic_action_cancel);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };

// set creator
        list.setMenuCreator(creator);

        list.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                selectedDrinks.remove(position);
                drinkAdapter.notifyDataSetChanged();
                Utils.setListViewHeightBasedOnChildren(list);
                onToggleTiltes();
                // false : close the menu; true : not close the menu
                return false;
            }
        });
        list.setSwipeDirection(SwipeMenuListView.DIRECTION_RIGHT);

        list.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);

        list.setScrollContainer(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        onToggleTiltes();
        onDateDisplay();
    }
    private void onToggleTiltes(){
        if (selectedDrinks.size() >= 1) {
            drinksTitle.setVisibility(View.VISIBLE);
            addDrinksTitle.setVisibility(View.GONE);
        }
        else{
            drinksTitle.setVisibility(View.GONE);
            addDrinksTitle.setVisibility(View.VISIBLE);
        }
    }

    private void onDateDisplay() {
        if (startDateDisplay != null && endDateDisplay != null) {
            if (startDateTime == null) {
                startDateDisplay.setText("");
            } else {
                startDateDisplay.setText(simpleDateFormat.format(startDateTime));
            }
            if (endDateTime == null) {
                endDateDisplay.setText("");
            } else {
                endDateDisplay.setText(simpleDateFormat.format(endDateTime));
            }
        }
    }

    private void show(final TextView displayView, boolean isStartTime)
    {
        SlideDateTimeListener listener = setListener(displayView, isStartTime);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Date initialDate = calendar.getTime();
        if(isStartTime && startDateTime != null){
            initialDate = startDateTime;
        }
        else if(!isStartTime && endDateTime != null){
            initialDate = endDateTime;
        }
        new SlideDateTimePicker.Builder(getSupportFragmentManager())
                .setListener(listener)
                .setInitialDate(initialDate)
//                        .setMinDate(minDateTime)
//                        .setMaxDate(maxDateTime)
                        //.setIs24HourTime(true)
                        //.setTheme(SlideDateTimePicker.HOLO_DARK)
                        //.setIndicatorColor(Color.parseColor("#990000"))
                .build()
                .show();

    }

    private SlideDateTimeListener setListener(final TextView displayView, final boolean isStartTime){

        return new SlideDateTimeListener() {

            @Override
            public void onDateTimeSet(Date date)
            {
                if(isStartTime){
                    startDateTime = date;
                }
                else{
                    endDateTime = date;
                }
                displayView.setText(simpleDateFormat.format(date));
            }
        };

    }

    @UiThread
    void setAlertDialog(String message){
        Utils.setAlertDialog(getString(R.string.warning), message, this).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
    }

    @Background
     void submitTemplate(ZonedDateTime startDateTime, ZonedDateTime endDateTime){
        ArrayList<Drink> drinksub = drinkServce.createDrink(selectedDrinks);
        try {
            apiService.saveAppSession(userPrefs.appId().get(),drinksub, startDateTime, endDateTime);
            dataSource.open();
            dataSource.createHistory(Calendar.getInstance().getTime());
            navigateFirstView();
        }
        catch(NetWorkException e){
            onError(e.getMessage());
        }
        catch(ServerValidationException e){
            String messages="";
            for(String message:e.getMessages()){
                messages += message + "\n";
            }
            onError(messages);
        }
        catch (Exception e){
            onError("Unexpected network error occured");
        }
        finally {
            pd.dismiss();
            dataSource.close();
        }
    }

    private void resetView(){
        selectedDrinks.clear();
        startDateTime = null;
        endDateTime = null;
        list.invalidateViews();
    }

    @UiThread
    void onError(String message){
        if(Utils.IsNullOrWhiteSpace(message)){
            message = "Unknow error occured while submission please close App and try later";
        }
        setAlertDialog(message);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_submit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id){
            case R.id.action_submit: {
                ZonedDateTime startZonedDateTime = Utils.convertDate(startDateTime);
                ZonedDateTime endZonedDateTime = Utils.convertDate(endDateTime);
                String result = boSubmit.validateSubmition(startZonedDateTime,endZonedDateTime,selectedDrinks);
                if(!result.equals("")){
                    onError(result);
                    return false;
                }
                onLoading();
                submitTemplate(startZonedDateTime, endZonedDateTime);
                break;
            }
            case android.R.id.home:{
                Utils.setAlertDialog(getResources().getString(R.string.warning),"The current template will be destroyed, continue?",this).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        navigateFirstView();
                    }
                }).setCancelable(true).setNegativeButton(android.R.string.cancel,new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }
    private void onLoading(){
        pd = new ProgressDialog(this);
        pd.setTitle("Processing...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);
        pd.setIndeterminate(true);
        pd.show();
    }
    @UiThread
    void navigateFirstView()
    {
        Intent firstViewIntent = new Intent(this, FirstVIewActivity_.class);
        firstViewIntent.putExtra("callerActivity", getString(R.string.title_activity_submit));
        firstViewIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        setResultIntent(firstViewIntent);
        resetView();
        this.startActivityForResult(firstViewIntent, 1);
        finish();

    }
    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }
    private void setResultIntent(Intent intent){
        if(intent == null) {
            intent = new Intent();
        }
        intent.putExtra("activity", false);
        setResult(RESULT_OK, intent);
    }
}
