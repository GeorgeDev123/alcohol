package au.org.checkmycontrol.capture;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.GridView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;

import au.org.checkmycontrol.capture.Adapter.DrinkTypeAdapter;
import au.org.checkmycontrol.capture.Model.DrinkType;

@EActivity(R.layout.activity_drink_type)
public class DrinkTypeActivity extends AppCompatActivity {
    @ViewById(R.id.gridview) GridView gridview;
    @ViewById(R.id.toolbar) Toolbar Toolbar;
    @Bean
    DrinkTypeAdapter adapter;

    @AfterViews
    void bindAdapter(){
        setTheme(R.style.AppDarkTheme);
        setSupportActionBar(Toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.primary_dark_material_dark));
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ff000000")));
            Toolbar.setTitleTextColor(Color.WHITE);
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_back);
        gridview.setAdapter(adapter);

    }
    @ItemClick(R.id.gridview)
    void gridViewItemClicked(DrinkType drinkType){
        Intent intent = new Intent(this, DrinkDetailsActivity_.class);
        intent.putExtra("Drink", drinkType);
        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_drink_type, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            Intent parentIntent = new Intent(this,DrinkDetailsActivity_ .class);
            parentIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(parentIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
