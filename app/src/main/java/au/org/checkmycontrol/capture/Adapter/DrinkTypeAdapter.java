package au.org.checkmycontrol.capture.Adapter;

import android.app.Activity;
import android.content.Context;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.ArrayList;

import au.org.checkmycontrol.capture.Model.DrinkType;
import au.org.checkmycontrol.capture.R;
import au.org.checkmycontrol.capture.ViewGroup.DrinkTypeItemView;
import au.org.checkmycontrol.capture.ViewGroup.DrinkTypeItemView_;
import au.org.checkmycontrol.capture.utils.DrinksTypesDataHolder;

/**
 * Created by zhi on 30/09/2015.
 */
@EBean
public class DrinkTypeAdapter extends BaseAdapter {
    @RootContext
    Context context;

    private ArrayList<DrinkType> drinks;
    int screenHeight;

    @AfterInject
    void initAdapter() {
        drinks = DrinksTypesDataHolder.getInstance(context).getData();
        screenHeight = ((Activity) context).getWindowManager()
                .getDefaultDisplay().getHeight();
    }

    @Override
    public int getCount() {
        return drinks.size();
    }

    @Override
    public DrinkType getItem(int position) {
        return drinks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DrinkTypeItemView drinkTypeItemView;
        if (convertView == null) {
            drinkTypeItemView = DrinkTypeItemView_.build(context);
        } else {
            drinkTypeItemView = (DrinkTypeItemView) convertView;
        }
        drinkTypeItemView.bind(getItem(position));
        TypedValue tv = new TypedValue();
        if (context.getTheme().resolveAttribute(R.attr.actionBarSize, tv, true)) {
            drinkTypeItemView.setMinimumHeight((screenHeight - TypedValue.complexToDimensionPixelSize(tv.data, context.getResources().getDisplayMetrics())) / 3);
        } else {
            drinkTypeItemView.setMinimumHeight(screenHeight / 3);
        }
        return drinkTypeItemView;
    }
}
