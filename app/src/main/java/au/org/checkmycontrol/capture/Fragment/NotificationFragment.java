package au.org.checkmycontrol.capture.Fragment;

import android.content.Context;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import org.androidannotations.annotations.AfterPreferences;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.PreferenceByKey;
import org.androidannotations.annotations.PreferenceChange;
import org.androidannotations.annotations.PreferenceScreen;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.util.Calendar;

import au.org.checkmycontrol.capture.Preference.TimePreference;
import au.org.checkmycontrol.capture.R;
import au.org.checkmycontrol.capture.Services.NotificationUtils;
import au.org.checkmycontrol.capture.Services.UserPrefs_;
import au.org.checkmycontrol.capture.utils.Utils;

@PreferenceScreen(R.xml.pref_general)
@EFragment
public class NotificationFragment extends PreferenceFragment {
    @PreferenceByKey(R.string.setting_morning)
    TimePreference preferenceMorning;

    @PreferenceByKey(R.string.setting_evening)
    TimePreference prefernceEvening;


    @PreferenceChange(R.string.setting_evening)
    void eveningPreferenceChange(Preference preference, String newValue){
        String[] time = newValue.split(":");
        int hour = Integer.valueOf(time[0]);
        int minute = Integer.valueOf(time[1]);
        userPrefs.eveningNotification().put(Integer.valueOf(hour).toString() + ":" + Integer.valueOf(minute).toString());
        prefernceEvening.setSummary(Utils.showTime(hour, minute));
        createOrUpdateAlarm(hour, minute, this.getActivity());
    }

    @Bean
    NotificationUtils notificationUtils;

    @Pref
    UserPrefs_ userPrefs;

    @AfterPreferences
    void initPrefs() {
        preferenceMorning.setSummary(Utils.convertTime(userPrefs.morningNotification().get()));
        preferenceMorning.setEnabled(false);
        prefernceEvening.setDefaultValue(userPrefs.eveningNotification().get());
        prefernceEvening.setSummary(Utils.convertTime(userPrefs.eveningNotification().get()));
    }

    @Background
    void createOrUpdateAlarm(int hourOfDay, int minute, Context context) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 0);
        notificationUtils.setNotification(calendar, context, notificationUtils.eveningTimeId);
    }
}
