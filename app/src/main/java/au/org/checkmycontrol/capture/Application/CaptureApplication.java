package au.org.checkmycontrol.capture.Application;

import android.app.Application;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.androidannotations.annotations.EApplication;

/**
 * Created by zhi on 29/09/2015.
 */
@EApplication
public class CaptureApplication extends Application {
    public static ObjectMapper objectMapper;

    public void onCreate() {
        super.onCreate();
        init();
    }

    private void init() {
        objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT,true)
                .findAndRegisterModules();
    }
}
