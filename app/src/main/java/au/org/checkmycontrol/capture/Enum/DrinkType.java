package au.org.checkmycontrol.capture.Enum;

/**
 * Created by zhi on 28/10/2015.
 */

import java.util.EnumSet;

import lombok.Getter;

import static au.org.checkmycontrol.capture.Enum.DrinkVolume.ML_120;
import static au.org.checkmycontrol.capture.Enum.DrinkVolume.ML_150;
import static au.org.checkmycontrol.capture.Enum.DrinkVolume.ML_200;
import static au.org.checkmycontrol.capture.Enum.DrinkVolume.ML_285;
import static au.org.checkmycontrol.capture.Enum.DrinkVolume.ML_30;
import static au.org.checkmycontrol.capture.Enum.DrinkVolume.ML_300;
import static au.org.checkmycontrol.capture.Enum.DrinkVolume.ML_330;
import static au.org.checkmycontrol.capture.Enum.DrinkVolume.ML_375;
import static au.org.checkmycontrol.capture.Enum.DrinkVolume.ML_425;
import static au.org.checkmycontrol.capture.Enum.DrinkVolume.ML_500;
import static au.org.checkmycontrol.capture.Enum.DrinkVolume.ML_570;
import static au.org.checkmycontrol.capture.Enum.DrinkVolume.ML_60;
import static au.org.checkmycontrol.capture.Enum.DrinkVolume.ML_90;
/**
 * Created by zhi on 28/10/2015.
 */
/**
 * Created by zhi on 5/10/2015.
 */
public enum DrinkType {
    BEER_FULL(4.8d, ML_200, ML_285, ML_375, ML_425, ML_570),
    BEER_LIGHT(2.7d, ML_200, ML_285, ML_375, ML_425, ML_570),
    BEER_MID(3.5d, ML_200, ML_285, ML_375, ML_425, ML_570),
    CIDER_PREMIX_SPIRIT(5d, ML_300, ML_330, ML_375, ML_500),
    COCKTAIL(40d, ML_30, ML_60, ML_90, ML_120),
    SPIRIT_LIQUEUR(40d, ML_30, ML_60),
    WINE_FORTIFIED(18d, ML_60),
    WINE_RED(13.5d, ML_150),
    WINE_WHITE_CHAMPAGNE(12d, ML_150);

    @Getter
    private final double alcoholPercentage;

    @Getter
    private final EnumSet<DrinkVolume> allowedVolumes;

    DrinkType(double alcoholPercentage, DrinkVolume first, DrinkVolume... rest) {
        this.alcoholPercentage = alcoholPercentage;
        this.allowedVolumes = EnumSet.of(first, rest);
    }
}


