package au.org.checkmycontrol.capture.Exception;

/**
 * Created by zhi on 12/10/2015.
 */
public class InvalidAppIdException extends Exception {
    public InvalidAppIdException(String message){
        super(message);
    }
}
