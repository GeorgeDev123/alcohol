package au.org.checkmycontrol.capture.Entities;

import au.org.checkmycontrol.capture.Enum.DrinkType;
import au.org.checkmycontrol.capture.Enum.DrinkVolume;
import lombok.Data;

/**
 * Created by zhi on 5/10/2015.
 */
@Data
public class Drink {
    private Long count;

    private DrinkType type;

    private DrinkVolume volume;
}
