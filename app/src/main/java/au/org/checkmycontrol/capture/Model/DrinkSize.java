package au.org.checkmycontrol.capture.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by zhi on 11/08/2015.
 */
public class DrinkSize implements Parcelable {
    public String size;
    public String volume;
    public String unit = "ml";

    public DrinkSize(String size, String volume) {
        this.size = size;
        this.volume = volume;
    }

    protected DrinkSize(Parcel in) {
        size = in.readString();
        volume = in.readString();
        unit = in.readString();
    }

    public static final Creator<DrinkSize> CREATOR = new Creator<DrinkSize>() {
        @Override
        public DrinkSize createFromParcel(Parcel in) {
            return new DrinkSize(in);
        }

        @Override
        public DrinkSize[] newArray(int size) {
            return new DrinkSize[size];
        }
    };

    public String getDrinkSizeDisplay(){
        return size + "(" + volume + unit +")";
    }


    public String getDrinkSizeSumbtionValue(){
        return unit.toUpperCase() + "_" + volume;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(size);
        dest.writeString(volume);
        dest.writeString(unit);
    }
}
