package au.org.checkmycontrol.capture.Entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.threetenbp.deser.LocalDateDeserializer;

import org.threeten.bp.LocalDate;

import lombok.Data;

/**
 * Created by zhi on 24/09/2015.
 */

@Data
public class Participant {
    @JsonProperty("id")
    private String id;
    @JsonProperty("alias")
    private String alias;
    @JsonProperty("primaryEmailAddress")
    private String primaryEmailAddress;
    @JsonProperty("secondaryEmailAddress")
    private String secondaryEmailAddress;
    @JsonProperty("dateOfBirth")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate dateOfBirth;
    @JsonProperty("capturePointOne")
    private CapturePointOne capturePointOne;
}


