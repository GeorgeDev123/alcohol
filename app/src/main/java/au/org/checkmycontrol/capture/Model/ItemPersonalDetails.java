package au.org.checkmycontrol.capture.Model;

/**
 * Created by zhi on 15/08/2015.
 */
public class ItemPersonalDetails {
    private String Title;
    private String Value;

    public ItemPersonalDetails(String title, String value){
        this.Title = title;
        this.Value = value;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }
}
