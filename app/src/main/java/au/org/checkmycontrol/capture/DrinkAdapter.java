package au.org.checkmycontrol.capture;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.ArrayList;

import au.org.checkmycontrol.capture.Model.DrinkSummary;
import au.org.checkmycontrol.capture.ViewGroup.DrinkSubmissionItemView;
import au.org.checkmycontrol.capture.ViewGroup.DrinkSubmissionItemView_;

/**
 * Created by zhi on 10/08/2015.
 */
@EBean
public class DrinkAdapter extends BaseAdapter {
    @RootContext
    Context context;

    private ArrayList<DrinkSummary> drinkSummaries = SubmitActivity.selectedDrinks;
    private final String symbol = ": ";

    @Override
    public int getCount() {
        return drinkSummaries.size();
    }

    @Override
    public DrinkSummary getItem(int position) {
        return drinkSummaries.get(position);
    }

    @Override
    public long getItemId(int postion) {
        return postion;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DrinkSubmissionItemView submissionItemView;
        if(convertView==null)
        {
            submissionItemView = DrinkSubmissionItemView_.build(context);
        }
        else
        {
            submissionItemView = (DrinkSubmissionItemView) convertView;
        }
        submissionItemView.bind(getItem(position));
        return submissionItemView;
    }
}
