package au.org.checkmycontrol.capture.Services;

import android.content.Context;

import com.fasterxml.jackson.core.JsonProcessingException;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.threeten.bp.LocalDate;

import java.io.IOException;

import au.org.checkmycontrol.capture.Application.CaptureApplication;
import au.org.checkmycontrol.capture.Entities.AppData;
import au.org.checkmycontrol.capture.Entities.Participant;

/**
 * Created by zhi on 12/10/2015.
 */
@EBean
public class AppServices {
    @RootContext
    Context context;

    @Pref
    UserPrefs_ userPrefs;

    @Bean
    ParticipantServices participantServices;
    public boolean isAppActive() throws IOException {
        if (!userPrefs.appActive().get()) {
            return false;
        }

        Participant participant = participantServices.getParticipant();
        if (LocalDate.now().isAfter(participant.getCapturePointOne().getAppData().getDeactivationDate())
                || LocalDate.now().isBefore(participant.getCapturePointOne().getAppData().getActivationDate())){
            return false;
        }

        return true;
    }
    public AppData getAppData(){
        AppData appData = null;
        try {
            if(userPrefs.appData().exists()) {
                appData = CaptureApplication.objectMapper.reader().forType(AppData.class).readValue(userPrefs.appData().get());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally{
            return appData;
        }
    }
    public void setAppData(AppData appData){
        try {
            userPrefs.appData().put(CaptureApplication.objectMapper.writer().forType(AppData.class).writeValueAsString(appData));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }
}