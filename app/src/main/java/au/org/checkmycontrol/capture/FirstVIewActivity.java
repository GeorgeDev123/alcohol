package au.org.checkmycontrol.capture;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AppOpsManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.DecimalFormat;

import au.org.checkmycontrol.capture.Entities.AppData;
import au.org.checkmycontrol.capture.Entities.AppStatus;
import au.org.checkmycontrol.capture.Exception.NetWorkException;
import au.org.checkmycontrol.capture.Exception.ServerValidationException;
import au.org.checkmycontrol.capture.Services.ApiServices;
import au.org.checkmycontrol.capture.Services.AppServices;
import au.org.checkmycontrol.capture.Services.NotificationUtils;
import au.org.checkmycontrol.capture.Services.ParticipantServices;
import au.org.checkmycontrol.capture.Services.UserPrefs_;
import au.org.checkmycontrol.capture.utils.Utils;

@EActivity(R.layout.activity_first_view)
public class FirstVIewActivity extends Activity {
    @ViewById(R.id.btn_firstview_yes) Button btnYes;
    @ViewById(R.id.btn_firstview_cancel) Button btnCancel;
    @ViewById(R.id.tv_firstview_question)TextView tvQuestion;
    @Click(R.id.btn_firstview_yes)
    void setClickBtnYesClick(){
        onLoading();
        setUsage(true);
    }
    @Click(R.id.btn_firstview_cancel)
    void setClickBtnCancel(){
        onLoading();
        setUsage(false);
    }
    @Click(R.id.tv_clickable_navigation)
    void setClickBtnAccount(){
        Intent accountIntent = new Intent(this, NavigationActivity_.class);
        accountIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivityForResult(accountIntent, 1);
    }
    @Click(R.id.tv_clickable_history)
    void setClickBtnHistory(){
        Intent historyIntent = new Intent(this, HistoryActivity_.class);
        startActivityForResult(historyIntent, 1);
    }

    @Bean
    NotificationUtils notificationUtils;
    @Pref
    UserPrefs_ userPrefs;
    @Bean
    ApiServices apiService;
    @Bean
    ParticipantServices participantServices;
    @Bean
    AppServices appServices;
    private ProgressDialog pd;
    private boolean reset = true;

    @AfterViews
    void setUp(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.primary_dark_material_dark));
        }
    }

    private void onFirstTime(){
        notificationUtils.setNotification(notificationUtils.getMorningNotificationCalendar(),this, notificationUtils.morningTimeId);
        notificationUtils.setNotification(notificationUtils.getEveningNotificationCalendar(),this, notificationUtils.eveningTimeId);
        userPrefs.firstTime().put(true);
    }

    private void onNoAppId(){
        Utils.setAlertDialog(getString(R.string.notification), getString(R.string.notification_message), this).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Utils.setAlertDialog(getString(R.string.first_time_launch), getString(R.string.please_log_in), FirstVIewActivity.this).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        navigateToLoginActivity();
                    }
                }).show();
            }
        }).show();
    }

    private void onNoNotificationEnabled(){
        Utils.setAlertDialog(getString(R.string.important), getString(R.string.no_notification), FirstVIewActivity.this).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                setEnableNotificaitonMessage();
                dialog.dismiss();
            }
        }).show();
    }

    private void navigateToLoginActivity(){
        Intent loginIntent = new Intent(this, LoginActivity_.class);
        startActivity(loginIntent);
    }

    private void navigateToDrinkTypeActivity(){
        Intent DrinkTypeIntent = new Intent(this, DrinkTypeActivity_.class);
        startActivity(DrinkTypeIntent);
    }
    
    @UiThread
    void setFinishMessage(){
        tvQuestion.setText(getString(R.string.thank_you) + "\n" + getString(R.string.close_the_app));
        btnYes.setVisibility(View.INVISIBLE);
        btnCancel.setVisibility(View.INVISIBLE);
    }

    @UiThread
    void setEnableNotificaitonMessage(){
        tvQuestion.setText(getString(R.string.enable_notification));
        btnYes.setVisibility(View.INVISIBLE);
        btnCancel.setVisibility(View.INVISIBLE);
    }

    @Background
    void setUsage(Boolean isConsumed){
        try {
            if(apiService.saveAppUsage(participantServices.getAppId(), isConsumed)){
                if(isConsumed) {
                    navigateToDrinkTypeActivity();
                }
                else{
                    setFinishMessage();
                }
            }
            else{
                onError("Connection problem");
            }
        }
        catch (NetWorkException e){
            onError(e.getMessage());
        }
        catch(ServerValidationException e){
            String messages="";
            for(String message:e.getMessages()){
                messages += message + "\n";
                if(message.contains("deactivated")){
                    getAppStatus();
                }
            }
            onError(messages);
        }
        catch (Exception e) {
            onError("Unkown error");
        }
        finally
        {
            pd.dismiss();
        }
    }

    @Background
    void getAppStatus(){
        AppStatus appStatus = null;
        try {
            appStatus = apiService.getAppStatus(participantServices.getAppId());
        } catch (NetWorkException e) {
            e.printStackTrace();
        } catch (ServerValidationException e) {
            onError(e.getMessage());
        }
        if(!appStatus.isActive()){
            appServices.setAppData(appStatus.getAppData());
            notificationUtils.CancelAllAlarms(this);
            onAppDeactive(appStatus.getAppData());
        }
    }

    @UiThread
    void onAppDeactive(AppData appData) {
        userPrefs.appActive().put(false);
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);

        String firstSentence = "The research period has now ended.\nThank you for participating.\n\n";
        if(appData != null) {
            String secondSentence = "Number of Standard Drinks:\n" + df.format(appData.getStandardDrinks() == null ? 0 : appData.getStandardDrinks()) + "\n\n";
            String thridSentence = "Number of Drinking Days:\n" + appData.getTotalDrinkingDays() + "\n\n";
            String fourthSentence = "Average Standard Drinks per day:\n" + df.format(appData.getAverageStandardDrinksPerDrinkingDay() == null ? 0 : appData.getAverageStandardDrinksPerDrinkingDay());
            tvQuestion.setText(firstSentence + secondSentence + thridSentence + fourthSentence);
        }
        else
        {
            tvQuestion.setText(firstSentence);
        }
        tvQuestion.setTextSize(18);
        btnYes.setVisibility(View.GONE);
        btnCancel.setVisibility(View.GONE);
    }

    @UiThread
    public void onError(String message){
        Utils.setAlertDialog("Warning", message, this).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_first_view, menu);
        return true;
    }
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!userPrefs.firstTime().exists() || !userPrefs.firstTime().get()){
            onFirstTime();
        }
        if(!userPrefs.participant().exists() || !userPrefs.appId().exists()){
            onNoAppId();
            return;
        }
        if(!userPrefs.appActive().get()){
            onAppDeactive(appServices.getAppData());
            return;
        }
        if(getIntent().getBooleanExtra("activity", false)){
            reset = false;
        }
        if(reset) {
            resetQuestion();
        }
        if(getIntent().hasExtra("callerActivity") && getIntent().getStringExtra("callerActivity").equalsIgnoreCase(getString(R.string.title_activity_submit))){
            setFinishMessage();
        }
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (!checkNotificationEnabled()) {
                onNoNotificationEnabled();
            }
        }
        try {
            if(!appServices.isAppActive()) {
                getAppStatus();
            }
        } catch (IOException e) {
            onError("Unexpected error occured");
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        reset = true;
        if(getIntent().hasExtra("callerActivity")){
            getIntent().removeExtra("callerActivity");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1){
            reset = false;
        }
    }

    private void resetQuestion(){
        tvQuestion.setText(getString(R.string.firstview_question));
        btnYes.setVisibility(View.VISIBLE);
        btnCancel.setVisibility(View.VISIBLE);
    }
    private void onLoading(){
        pd = new ProgressDialog(this);
        pd.setTitle("Processing...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);
        pd.setIndeterminate(true);
        pd.show();
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private boolean checkNotificationEnabled(){
        String CHECK_OP_NO_THROW = "checkOpNoThrow";
        String OP_POST_NOTIFICATION = "OP_POST_NOTIFICATION";

        AppOpsManager mAppOps = (AppOpsManager) getSystemService(Context.APP_OPS_SERVICE);
        ApplicationInfo appInfo = getApplicationInfo();
        String pkg = getApplicationContext().getPackageName();
        int uid = appInfo.uid;
        Class appOpsClass = null; /* Context.APP_OPS_MANAGER */
        try {
            appOpsClass = Class.forName(AppOpsManager.class.getName());
            Method checkOpNoThrowMethod = appOpsClass.getMethod(CHECK_OP_NO_THROW, Integer.TYPE, Integer.TYPE, String.class);
            Field opPostNotificationValue = appOpsClass.getDeclaredField(OP_POST_NOTIFICATION);
            int value = (int)opPostNotificationValue.get(Integer.class);
            int mode = (Integer) checkOpNoThrowMethod.invoke(mAppOps,value, uid, pkg);
            return (mode == AppOpsManager.MODE_ALLOWED);
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
    }
}
