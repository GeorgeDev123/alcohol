package au.org.checkmycontrol.capture.Services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.io.IOException;

import au.org.checkmycontrol.capture.Application.CaptureApplication;
import au.org.checkmycontrol.capture.Entities.Participant;

/**
 * Created by zhi on 26/09/2015.
 */
@EBean
public class ParticipantServices {
    @Pref
    UserPrefs_ userPrefs;

    public Participant createOrUpdateParticipant(Participant participant) throws IOException, JsonProcessingException {
        if(participant == null)
            return null;
        ObjectMapper mapper = CaptureApplication.objectMapper;
        ObjectWriter objectWriter = mapper.writer().forType(Participant.class);
        String localParticipant = objectWriter.writeValueAsString(participant);
        userPrefs.participant().put(localParticipant);
        userPrefs.appId().put(participant.getCapturePointOne().getAppData().getAppId());
        return participant;
    }

    public Participant getParticipant() throws IOException, JsonProcessingException{
        return CaptureApplication.objectMapper.reader().forType(Participant.class).readValue(userPrefs.participant().get());
    }

    public String getParticipantId() throws IOException, JsonProcessingException{
        return getParticipant().getId();
    }

    public String getAppId()
    {
        return userPrefs.appId().get();
    }
}
