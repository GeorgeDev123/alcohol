package au.org.checkmycontrol.capture.Services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.Calendar;

/**
 * Created by zhizheng on 24/08/2015.
 */

public class NotificationReceiver extends BroadcastReceiver {

    NotificationUtils notificationUtils;

    @Override
    public void onReceive(Context context, Intent intent)
    {
        Intent service = new Intent(context, NotificationServices.class);
        Log.d("On Receive", "Receive");
        if(intent.getIntExtra(NotificationServices.NOTIFY_ID,0)  !=0) {
            int hour = intent.getIntExtra(NotificationServices.NOTIFY_HOUR, 22);
            int minute = intent.getIntExtra(NotificationServices.NOTIFY_Minutes,0);
            Calendar newCalendar = Calendar.getInstance();

            newCalendar.set(Calendar.HOUR_OF_DAY, hour);
            newCalendar.set(Calendar.MINUTE,minute);
            newCalendar.set(Calendar.SECOND, 0);
            newCalendar.add(Calendar.DATE, 1);
            notificationUtils = new NotificationUtils();
            notificationUtils.setNotification(newCalendar, context,intent.getIntExtra(NotificationServices.NOTIFY_ID,0));
            service.putExtra(NotificationServices.INTENT_NOTIFY, true);
            service.putExtra(NotificationServices.NOTIFY_ID, intent.getIntExtra(NotificationServices.NOTIFY_ID,0));
            context.startService(service);
        }
    }
}
