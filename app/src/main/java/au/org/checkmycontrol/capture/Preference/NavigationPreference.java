package au.org.checkmycontrol.capture.Preference;

import android.content.Context;
import android.preference.Preference;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import au.org.checkmycontrol.capture.R;
import au.org.checkmycontrol.capture.utils.Utils;

/**
 * Created by zhi on 8/10/2015.
 */
public class NavigationPreference extends Preference {

    public NavigationPreference(Context context) {
        super(context);
    }

    public NavigationPreference(Context context, AttributeSet attrs) {
        super(context,attrs);
    }

    public NavigationPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs,defStyleAttr);
    }

    public NavigationPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected View onCreateView(ViewGroup parent) {
        super.onCreateView(parent);
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return li.inflate(R.layout.item_navigation, parent, false);
    }

    @Override
    public View getView(View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = onCreateView(parent);
        }
        onBindView(convertView);
        return convertView;
    }

    protected void onBindView(View view) {
        super.onBindView(view);
        final TextView titleView = (TextView) view.findViewById(R.id.item_navigation_title);
        if (titleView != null) {
            final CharSequence title = getTitle();
            if (!TextUtils.isEmpty(title)) {
                titleView.setText(title);
                titleView.setVisibility(View.VISIBLE);
            } else {
                titleView.setVisibility(View.GONE);
            }
        }

        final TextView summaryView = (TextView) view.findViewById(R.id.item_navigation_summary);
        if (summaryView != null) {
            final CharSequence summary = getSummary();
            if (!TextUtils.isEmpty(summary)) {
                summaryView.setText(summary);
                summaryView.setVisibility(View.VISIBLE);
            } else {
                summaryView.setVisibility(View.GONE);
            }
        }
        final ImageView imageView = (ImageView) view.findViewById(R.id.img_navigation);

        if (imageView != null) {
            super.setIcon(R.drawable.ic_action_forward);
            imageView.setImageDrawable(getIcon());
            imageView.setVisibility(getIcon() != null ? View.VISIBLE : View.GONE);
        }
    }
}
