package au.org.checkmycontrol.capture.Entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * Created by zhi on 12/10/2015.
 */
@Data
public class AppStatus {
    boolean isActive;
    @JsonProperty("appData")
    AppData appData;
    String status;
}
