package au.org.checkmycontrol.capture.Entities;

import org.threeten.bp.ZonedDateTime;

import java.util.ArrayList;
import java.util.List;

import au.org.checkmycontrol.capture.Enum.AppVersion;
import lombok.Data;

/**
 * Created by zhi on 5/10/2015.
 */
@Data
public class Session {

    private AppVersion appVersion;
    private ZonedDateTime startedAt;
    private ZonedDateTime endedAt;
    private List<Drink> drinks = new ArrayList<>();

}
