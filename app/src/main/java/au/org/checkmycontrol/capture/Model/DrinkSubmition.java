package au.org.checkmycontrol.capture.Model;

/**
 * Created by zhizheng on 19/08/2015.
 */
public class DrinkSubmition {
    int count;
    String type;
    String volume;

    public DrinkSubmition(DrinkSummary summary){
        this.count = summary.quantity;
        this.type = summary.submitEnum;
        this.volume = summary.submitSize;
    }
}
