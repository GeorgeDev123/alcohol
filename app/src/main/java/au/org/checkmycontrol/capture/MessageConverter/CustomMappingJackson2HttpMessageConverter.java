package au.org.checkmycontrol.capture.MessageConverter;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import au.org.checkmycontrol.capture.Application.CaptureApplication;

/**
 * Created by zhi on 29/09/2015.
 */
public class CustomMappingJackson2HttpMessageConverter extends MappingJackson2HttpMessageConverter {
    public CustomMappingJackson2HttpMessageConverter(){
        setObjectMapper(CaptureApplication.objectMapper.
                configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true).
                configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false).
                configure(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS, false).
                configure(SerializationFeature.WRITE_DATES_WITH_ZONE_ID,true));
    }
}
