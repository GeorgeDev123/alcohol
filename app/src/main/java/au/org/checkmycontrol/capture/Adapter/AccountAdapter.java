package au.org.checkmycontrol.capture.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import au.org.checkmycontrol.capture.Model.ItemPersonalDetails;
import au.org.checkmycontrol.capture.R;

/**
 * Created by zhi on 10/08/2015.
 */
public class AccountAdapter extends ArrayAdapter<ItemPersonalDetails> {
    private Context mContext;
    private ArrayList<ItemPersonalDetails> listAccountInfo = new ArrayList<ItemPersonalDetails>();
    private String sizeLabel;
    private String quantityLabel;
    private final String symbol = ": ";

    public AccountAdapter(Context context, ArrayList<ItemPersonalDetails> listAccountInfo ){
        super(context, R.layout.item_account_details ,listAccountInfo);
        this.mContext = context;
        this.listAccountInfo = listAccountInfo;
    }

    @Override
    public int getCount() {
        return listAccountInfo.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null)
        {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_account_details, parent,false);
        }
        TextView tvTitle = (TextView) convertView.findViewById(R.id.tv_account_detail_title);
        TextView tvValue = (TextView) convertView.findViewById(R.id.tv_account_detail_value);

        ItemPersonalDetails item = listAccountInfo.get(position);
        tvTitle.setText(item.getTitle());
        tvValue.setText(item.getValue());

        return convertView;
    }

}
