package au.org.checkmycontrol.capture;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_navigation)
public class NavigationActivity extends AppCompatActivity {
    @ViewById(R.id.navigation_about)
    RelativeLayout layoutNavigationAbout;
    @Click(R.id.navigation_about)
    void navigateAbout(){
        Intent aboutIntent = new Intent(this,AboutActivity_.class);
        aboutIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(aboutIntent);
    }
    @ViewById(R.id.navigation_participation)
    RelativeLayout layoutNavigationParticipation;
    @Click(R.id.navigation_participation)
    void navigateParticipant(){
        Intent accountIntent = new Intent(this, AccountActivity_.class);
        startActivityForResult(accountIntent, 1);
    }
    @ViewById(R.id.navigation_notification)
    RelativeLayout layoutNavigationNotification;
    @Click(R.id.navigation_notification)
    void navigateNotification(){
        Intent settingIntent = new Intent(this, SettingActivity_.class);
        startActivity(settingIntent);
    }
    @ViewById(R.id.toolbar) Toolbar mToolbar;
    TextView navigationAboutTitle;
    TextView navigationParticipationTitle;
    TextView navigationNotificationTitle;

    @AfterViews
    void initView(){
        navigationAboutTitle = (TextView) layoutNavigationAbout.findViewById(R.id.item_navigation_title);
        navigationParticipationTitle = (TextView) layoutNavigationParticipation.findViewById(R.id.item_navigation_title);
        navigationNotificationTitle = (TextView) layoutNavigationNotification.findViewById(R.id.item_navigation_title);
        navigationAboutTitle.setText(getString(R.string.about));
        navigationParticipationTitle.setText(getString(R.string.participant_information));
        navigationNotificationTitle.setText(getString(R.string.notification_reminder));
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_navigation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            Intent homeIntent = new Intent(this,FirstVIewActivity_.class);
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            setResultIntent(homeIntent);
            this.startActivityForResult(homeIntent, 1);
        }

        return super.onOptionsItemSelected(item);
    }
    private void setResultIntent(Intent intent){
        if(intent == null) {
            intent = new Intent();
        }
        intent.putExtra("activity", false);
        setResult(RESULT_OK, intent);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResultIntent(null);
    }
}
