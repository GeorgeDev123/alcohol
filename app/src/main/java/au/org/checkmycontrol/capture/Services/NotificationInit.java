package au.org.checkmycontrol.capture.Services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by zhi on 22/10/2015.
 */
public class NotificationInit extends BroadcastReceiver {
    NotificationUtils_ notificationUtils_;

    @Override
    public void onReceive(Context context, Intent intent) {
        notificationUtils_ = NotificationUtils_.getInstance_(context);
        if(notificationUtils_.userPrefs.appActive().get()) {
            notificationUtils_.setNotification(notificationUtils_.getMorningNotificationCalendar(), context, notificationUtils_.morningTimeId);
            notificationUtils_.setNotification(notificationUtils_.getEveningNotificationCalendar(), context, notificationUtils_.eveningTimeId);
        }
    }
}
