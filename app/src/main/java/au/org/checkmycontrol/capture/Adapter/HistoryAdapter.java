package au.org.checkmycontrol.capture.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;

import au.org.checkmycontrol.capture.Model.SubmitionHistory;
import au.org.checkmycontrol.capture.R;
import au.org.checkmycontrol.capture.utils.Utils;

/**
 * Created by zhi on 10/08/2015.
 */
public class HistoryAdapter extends ArrayAdapter<SubmitionHistory> {
    private Context mContext;
    private ArrayList<SubmitionHistory> listHistory = new ArrayList<SubmitionHistory>();
    private String sizeLabel;
    private String quantityLabel;
    private final String symbol = ": ";

    public HistoryAdapter(Context context, ArrayList<SubmitionHistory> listHistory){
        super(context, R.layout.item_history ,listHistory);
        this.mContext = context;
        this.listHistory = listHistory;
    }

    @Override
    public int getCount() {
        return listHistory.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null)
        {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_history, parent,false);
        }
        TextView tvTitle = (TextView) convertView.findViewById(R.id.tv_history_date);
        TextView tvValue = (TextView) convertView.findViewById(R.id.tv_history_time);

        SubmitionHistory item = listHistory.get(position);
        Date date = item.getDate();
        tvTitle.setText(Utils.showOnlyDate(date));
        tvValue.setText(Utils.showOnlyTime(date));

        return convertView;
    }

}
