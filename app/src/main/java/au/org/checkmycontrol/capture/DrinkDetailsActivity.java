package au.org.checkmycontrol.capture;

import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import au.org.checkmycontrol.capture.Fragment.DetailsFragment;
import au.org.checkmycontrol.capture.Fragment.DetailsFragment_;
import au.org.checkmycontrol.capture.Model.DrinkSummary;
import au.org.checkmycontrol.capture.Model.DrinkType;
import au.org.checkmycontrol.capture.Services.DrinkServices;

@EActivity(R.layout.activity_drink_details)
public class DrinkDetailsActivity extends AppCompatActivity {
    @ViewById(R.id.layout_drink_details_size)  RelativeLayout size;
    DetailsFragment detailsFragment;
    @ViewById(R.id.toolbar) Toolbar mToolbar;
    @Bean
    DrinkServices drinkService;

    @AfterViews
    void setUp() {
        Intent i = getIntent();
        if (getIntent().hasExtra("Drink")) {
            detailsFragment = DetailsFragment_.builder().drinkType((DrinkType) i.getParcelableExtra("Drink")).build();
        } else {
            detailsFragment = DetailsFragment_.builder().build();
        }
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frame_drink_detail, detailsFragment);
        ft.commit();

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private boolean navigateSubmitActivity(){
        DrinkSummary drinkSummary = drinkService.createOrUpdateDrinkSummary(detailsFragment.getDrinkType(), detailsFragment.getSize(), detailsFragment.getQuantity());
        if(drinkSummary == null){
            showUnfinishDialog();
            return false;
        }
        Intent intent = new Intent(DrinkDetailsActivity.this,SubmitActivity_.class);
        intent.putExtra("Drink", drinkSummary);
        startActivity(intent);
        finish();
        return true;
    }

    private void showUnfinishDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setTitle("Note")
                .setMessage("Unfinished data field")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setCancelable(false)
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_drink_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id){
            case R.id.action_navigate:
                return navigateSubmitActivity();
        }

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            Intent upIntent = new Intent(this, SubmitActivity_.class);
            upIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(upIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
