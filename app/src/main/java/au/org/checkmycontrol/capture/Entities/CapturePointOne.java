package au.org.checkmycontrol.capture.Entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * Created by zhi on 29/09/2015.
 */
@Data
public class CapturePointOne {
    @JsonProperty("id")
    private String id;
    @JsonProperty("appData")
    private AppData appData;
    @JsonProperty("demographicQuestionnaire")
    private DemographicQuestionnaire demographicQuestionnaire;
}
