package au.org.checkmycontrol.capture.Services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import au.org.checkmycontrol.capture.FirstVIewActivity_;
import au.org.checkmycontrol.capture.R;

/**
 * Created by zhizheng on 24/08/2015.
 */

public class NotificationServices extends Service{

    public static final String INTENT_NOTIFY = "Capture notification";
    public static final String NOTIFY_ID = "Notification ID";
    public static final String NOTIFY_HOUR = "Hour";
    public static final String NOTIFY_Minutes = "Minutes";

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        if(intent.getBooleanExtra(INTENT_NOTIFY,false)){
            showNotification(intent.getIntExtra(NOTIFY_ID,0));
        }
        stopSelf();
        return START_NOT_STICKY;
    }

    private void showNotification(int notifyId){
        NotificationManager mManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Intent intent1 = new Intent(this.getApplicationContext(),FirstVIewActivity_.class);
        PendingIntent pendingNotificationIntent = PendingIntent.getActivity( this.getApplicationContext(),0, intent1,PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mNotifyBuilder = new NotificationCompat.Builder(
                this).setSmallIcon(R.drawable.ic_notifications_white)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher))
                .setContentTitle("Capture")
                .setContentText("Please record activity in the Capture app now")
                .setContentIntent(pendingNotificationIntent)
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});

        Notification notification = mNotifyBuilder.build();
        notification.flags = Notification.FLAG_AUTO_CANCEL;
        mManager.notify(notifyId, notification);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
