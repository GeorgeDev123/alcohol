package au.org.checkmycontrol.capture;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import au.org.checkmycontrol.capture.Entities.Participant;
import au.org.checkmycontrol.capture.Exception.NetWorkException;
import au.org.checkmycontrol.capture.Exception.ServerValidationException;
import au.org.checkmycontrol.capture.Services.ApiServices;
import au.org.checkmycontrol.capture.Services.NotificationUtils;
import au.org.checkmycontrol.capture.Services.ParticipantServices;
import au.org.checkmycontrol.capture.utils.Utils;

@EActivity(R.layout.activity_login)
public class LoginActivity extends AppCompatActivity {
    @Bean
    ParticipantServices participantServices;

    @Bean
    public ApiServices apiService;

    @Bean
    NotificationUtils notificationUtils;

    @ViewById(R.id.toolbar)
    Toolbar mToolbar;

    @ViewById(R.id.btn_log_in)
    Button btnLogin;

    @ViewById(R.id.et_appid)
    EditText etAppId;

    @Click(R.id.btn_log_in)
    public void logIn() {
        String appId = etAppId.getText().toString();

        if (StringUtils.isBlank(appId)) {
            onError("Invalid AppId");
            return;
        }

        validAppIdOverNetwork(appId);
    }

    @AfterViews
    void setUp(){
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @UiThread
     void onError(String message){
        Utils.setAlertDialog(getString(R.string.warning), message, LoginActivity.this).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            Intent parentIntent = new Intent(this,FirstVIewActivity_.class);
            parentIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(parentIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void btnConfirmListener(View view) {
        Intent intent = new Intent(LoginActivity.this, FirstVIewActivity.class);
        startActivity(intent);
    }

    @Background
    void validAppIdOverNetwork(String appId) {
        try {
            Participant participant = participantServices.createOrUpdateParticipant(apiService.validAppId(appId));
            if(participant != null) {
//                notificationUtils.setNotification(notificationUtils.getMorningNotificationCalendar(),this, notificationUtils.morningTimeId);
//                notificationUtils.setNotification(notificationUtils.getEveningNotificationCalendar(),this, notificationUtils.eveningTimeId);
                onLoginFinished();
            }
            else{
                throw new Exception("Incorrect appId");
            }
        }
        catch (NetWorkException e){
            onError(e.getMessage());
        }
        catch(HttpClientErrorException e){
            if(e.getStatusCode() == HttpStatus.UNAUTHORIZED){
                onError("Incorrect appID");
            }
            else{
                onError("Unable to retrieve your details");
            }
        }
        catch(ServerValidationException e){
            onError("App is deactivated");
        }
        catch (Exception e) {
            onError("Unable to retrieve your details");
        }
    }

    void onLoginFinished() {
        Intent firstViewActivityIntent = new Intent(LoginActivity.this, FirstVIewActivity_.class);
        startActivity(firstViewActivityIntent);
        finish();
    }
}
