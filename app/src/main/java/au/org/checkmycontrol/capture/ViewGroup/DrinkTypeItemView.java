package au.org.checkmycontrol.capture.ViewGroup;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import au.org.checkmycontrol.capture.Model.DrinkType;
import au.org.checkmycontrol.capture.R;

/**
 * Created by zhi on 30/09/2015.
 */
@EViewGroup(R.layout.item_drink_type)
public class DrinkTypeItemView extends LinearLayout {
    @ViewById(R.id.tv_drink_type_name)
    TextView tvDrinkTypeName;
    @ViewById(R.id.img_drink_type_item)
    ImageView imgDrinkType;

    public DrinkTypeItemView(Context context) {
        super(context);
    }
    public void bind(DrinkType drinkType) {
        imgDrinkType.setImageResource(drinkType.getImageId());
        if(drinkType.getDrinkType().equals("Beer (Light Strength)")) {
            imgDrinkType.setAlpha(200);
        }
        else if(drinkType.getDrinkType().equals("Beer (Mid Strength)")){
            imgDrinkType.setAlpha(225);
        }
        tvDrinkTypeName.setText(drinkType.getDrinkType());
    }
}
