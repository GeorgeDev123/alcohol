package au.org.checkmycontrol.capture.Services;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.rest.RestService;
import org.apache.commons.codec.binary.Hex;
import org.threeten.bp.ZonedDateTime;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;

import au.org.checkmycontrol.capture.Application.CaptureApplication;
import au.org.checkmycontrol.capture.Entities.AppData;
import au.org.checkmycontrol.capture.Entities.AppStatus;
import au.org.checkmycontrol.capture.Entities.AppUsage;
import au.org.checkmycontrol.capture.Entities.Drink;
import au.org.checkmycontrol.capture.Entities.Participant;
import au.org.checkmycontrol.capture.Entities.Session;
import au.org.checkmycontrol.capture.Exception.InvalidAppIdException;
import au.org.checkmycontrol.capture.Exception.NetWorkException;
import au.org.checkmycontrol.capture.Exception.ServerValidationException;
import au.org.checkmycontrol.capture.Enum.AppVersion;
import lombok.val;
/**
 * Created by zhi on 29/09/2015.
 */
@EBean
public class ApiServices {
    @RootContext
    Context mContext;

    @RestService
    public IREST apiService;
    @App
    CaptureApplication captureApplication;

    private final String appSalt = "6ecccb44-6122-4e15-a620-5db90829fd18";
    private final String ACTIVE = "active";

    private Calendar calendar = Calendar.getInstance();

    public Participant validAppId(String appId) throws Exception{
        if(!isNetworkConnected()){
            throw new NetWorkException("No network is discovered");
        }
        apiService.setHttpBasicAuth(appId, generatePassword(appId));
        Object result = apiService.getAppId(appId);
        if(result == null){
            throw new InvalidAppIdException("Invalid appID");
        }
        Participant participant = captureApplication.objectMapper.convertValue(result, Participant.class);
        if(participant.getId() == null){
            throw new ServerValidationException("App is deactivated");
        }
        return participant;
    }
    public boolean saveAppUsage(String appId, boolean isConsumed) throws ServerValidationException, Exception {
        if (!isNetworkConnected()) {
            throw new NetWorkException("No network is discovered");
        }
        val appUsage = new AppUsage();
        appUsage.setAppVersion(AppVersion.V_1_0);
        appUsage.setAlcoholConsumed(isConsumed);
        apiService.setHttpBasicAuth(appId, generatePassword(appId));
        Object object = apiService.saveAppUsage(appId, appUsage);
        if (object == null) {
            return true;
        }
        ArrayList<String> errorMessages = getErrorMessage(object);
        if (errorMessages.size() > 0) {
            throw new ServerValidationException(errorMessages);
        }
        return false;
    }

    public boolean saveAppSession(String appId, ArrayList<Drink> drinks, ZonedDateTime startDate, ZonedDateTime endDate) throws Exception {
        if (!isNetworkConnected()) {
            throw new NetWorkException("No network is discovered");
        }

        val session = new Session();
        session.setAppVersion(AppVersion.V_1_0);
        session.setDrinks(drinks);
        session.setStartedAt(startDate);
        session.setEndedAt(endDate);
        apiService.setHttpBasicAuth(appId, generatePassword(appId));
        Object object = apiService.saveAppSession(appId, session);
        if (object == null) {
            return true;
        }
        ArrayList<String> errorMessages = getErrorMessage(object);
        if (errorMessages.size() > 0) {
            throw new ServerValidationException(errorMessages);
        }
        return false;
    }

    public AppStatus getAppStatus(String appId) throws NetWorkException, ServerValidationException {
        if (!isNetworkConnected()) {
            throw new NetWorkException("No network is discovered");
        }
        apiService.setHttpBasicAuth(appId, generatePassword(appId));
        Object result = apiService.checkAppStatus(appId);
        AppStatus appStatus = new AppStatus();
        AppData appData = captureApplication.objectMapper.convertValue(result, AppData.class);
        if(appData.getAppId() == null){
            appStatus = captureApplication.objectMapper.convertValue(result, AppStatus.class);
            if(appStatus.getStatus().equals(ACTIVE)){
                appStatus.setActive(true);
                return appStatus;
            }
            else{
                throw new ServerValidationException("Invalid AppID");
            }
        }
        appStatus.setActive(false);
        appStatus.setAppData(appData);
        return appStatus;
    }

    private boolean isNetworkConnected(){
        ConnectivityManager connMgr = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
    private String generatePassword(final String appID) {
        final String message = appID + appSalt;

        try {
            final MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(message.getBytes());

            byte[] digest = messageDigest.digest();
            return new String(Hex.encodeHex(digest));
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException(e);
        }
    }
    private ArrayList<String> getErrorMessage(Object error){
        ArrayList<String> errorMessages = new ArrayList<>();
        try {
            if (error instanceof LinkedHashMap) {
                ArrayList messageList = (ArrayList) ((LinkedHashMap) error).get("errors");
                if(messageList.size() == 1){
                    errorMessages.add(((LinkedHashMap <String,String>)messageList.get(0)).get("message"));
                }
                else {
                    for (int i = 0; i < messageList.size(); i++) {
                        for (String key : ((LinkedHashMap<String, String>) messageList.get(i)).keySet()) {
                            errorMessages.add(key + " " + ((LinkedHashMap) messageList.get(i)).get(key).toString());
                        }
                    }
                }
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            return errorMessages;
        }
    }
}
