package au.org.checkmycontrol.capture.Model;

import android.os.Parcel;
import android.os.Parcelable;

import lombok.Data;

/**
 * Created by zhi on 10/08/2015.
 */
@Data
public class DrinkSummary implements Parcelable {
    DrinkType drinkType;
    Integer imageSrc;
    int quantity;
    String size;
    String submitEnum;
    String submitSize;

    public DrinkSummary(String size, Integer quantity, Integer imageSrc, String submitEnum, String submitSize){
        this.size = size;
        this.quantity = quantity;
        this.imageSrc = imageSrc;
        this.submitEnum = submitEnum;
        this.submitSize = submitSize;
    }

    public DrinkSummary(){

    }

    protected DrinkSummary(Parcel in) {
        drinkType = (DrinkType) in.readValue(DrinkType.class.getClassLoader());
        imageSrc = in.readInt();
        quantity = in.readInt();
        size = in.readString();
        submitEnum = in.readString();
        submitSize = in.readString();
    }

    public static final Creator<DrinkSummary> CREATOR = new Creator<DrinkSummary>() {
        @Override
        public DrinkSummary createFromParcel(Parcel in) {
            return new DrinkSummary(in);
        }

        @Override
        public DrinkSummary[] newArray(int size) {
            return new DrinkSummary[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(drinkType);
        dest.writeInt(imageSrc);
        dest.writeInt(quantity);
        dest.writeString(size);
        dest.writeString(submitEnum);
        dest.writeString(submitSize);
    }
}
