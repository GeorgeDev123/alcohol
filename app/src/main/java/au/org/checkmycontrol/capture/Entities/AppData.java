package au.org.checkmycontrol.capture.Entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.threeten.bp.LocalDate;

import lombok.Data;

/**
 * Created by zhi on 25/09/2015.
 */
@Data
public class AppData {
    @JsonProperty("id")
    private String id;
    @JsonProperty("participant")
    private Participant participant;
    @JsonProperty("appId")
    private String appId;
    @JsonProperty("activationDate")
    private LocalDate activationDate;
    @JsonProperty("deactivationDate")
    private LocalDate deactivationDate;
    @JsonProperty("totalDrinks")
    public Long totalDrinks;
    @JsonProperty("standardDrinks")
    public Double standardDrinks;
    @JsonProperty("totalDrinkingDays")
    public Long totalDrinkingDays;
    @JsonProperty("averageStandardDrinksPerDrinkingDay")
    public Double averageStandardDrinksPerDrinkingDay;
}
