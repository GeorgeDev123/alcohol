package au.org.checkmycontrol.capture.Preference;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.Preference;
import android.util.AttributeSet;
import android.widget.TextView;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by zhi on 4/10/2015.
 */
public class DateTimePreference extends Preference {
    private int lastHour=0;
    private int lastMinute=0;
    private SlideDateTimePicker picker=null;
//    @Getter
//    private TextView tv;
    public static int getHour(String time) {
        String[] pieces=time.split(":");

        return(Integer.parseInt(pieces[0]));
    }

    public static int getMinute(String time) {
        String[] pieces=time.split(":");

        return(Integer.parseInt(pieces[1]));
    }

    public DateTimePreference(Context ctxt, AttributeSet attrs) {
        super(ctxt, attrs);
    }
    private SlideDateTimeListener setListener(final TextView displayView){

        return new SlideDateTimeListener() {

            @Override
            public void onDateTimeSet(Date date)
            {
                SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.getDefault());
                displayView.setText(simpleDateFormat.format(date));
            }
        };

    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return(a.getString(index));
    }

    @Override
    protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {
//        String time=null;
//
//        if (restoreValue) {
//            if (defaultValue==null) {
//                time=getPersistedString("00:00");
//            }
//            else {
//                time=getPersistedString(defaultValue.toString());
//            }
//        }
//        else {
//            time=defaultValue.toString();
//        }
//
//        lastHour=getHour(time);
//        lastMinute=getMinute(time);
    }
}
