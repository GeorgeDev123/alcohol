package au.org.checkmycontrol.capture.Services;

import org.androidannotations.annotations.EBean;
import org.threeten.bp.Clock;
import org.threeten.bp.Duration;
import org.threeten.bp.ZonedDateTime;
import org.threeten.bp.temporal.ChronoUnit;

import java.util.ArrayList;

import au.org.checkmycontrol.capture.utils.Utils;

/**
 * Created by zhi on 22/08/2015.
 */
@EBean
public class BOSubmit {
    private static final int BLOCK_MINUTES = 15;
    public String validateSubmition(ZonedDateTime startDate, ZonedDateTime endDate, ArrayList selectedDrinks) {
        String result = "";
        if(startDate == null || endDate == null){
            return "please specifc a date for start date and end date";
        }
        result= datetimeValidation(startDate, endDate);
        if (result != "") {
            return result;
        }
        result = DrinkValidation(selectedDrinks);
        if(result != ""){
            return result;
        }
        return "";
    }

    public String datetimeValidation(ZonedDateTime startDate, ZonedDateTime endDate) {
        Clock clock = Utils.getClock();
        ZonedDateTime now = ZonedDateTime.now(clock);
        Duration duration = Duration.between(startDate, endDate);
        if(endDate.isAfter(getNextTimeBlock(now)))
        {
            return "End date or time must not greater than current date or time";
        }
        else if(startDate.isAfter(endDate)){
            return "Your end date or time is set before start date or time";
        }
        else if(duration.compareTo(Duration.ofMinutes(15)) < 0){
            return "Drinking period must be more or equal to 15 minutes";
        }
        else if(duration.compareTo(Duration.ofHours(10)) > 0){
            return "Drinking period must be less than 10 hours";
        }
        else if(startDate.isBefore(now.minusHours(24)) || startDate.isAfter(getCurrentTimeBlock(now))){
            return "Cannot submit data more than 24 hours ago";
        }
        return "";
    }

    public String DrinkValidation(ArrayList selectedDrinks){
        if(selectedDrinks.size() <=0){
            return "You haven't consumed any drinks";
    }
    return "";
    }

    private ZonedDateTime getCurrentTimeBlock(final ZonedDateTime now) {
        final int currentMinute = now.getMinute();

        final int nextBlockMinute = (int) Math.ceil(currentMinute / (double) BLOCK_MINUTES) * BLOCK_MINUTES;

        return now.plusMinutes(nextBlockMinute - currentMinute).truncatedTo(ChronoUnit.MINUTES);
    }

    private ZonedDateTime getNextTimeBlock(final ZonedDateTime now) {
        return getCurrentTimeBlock(now).plusMinutes(BLOCK_MINUTES);
    }
}
