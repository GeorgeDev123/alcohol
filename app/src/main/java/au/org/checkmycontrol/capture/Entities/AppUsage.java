package au.org.checkmycontrol.capture.Entities;

import au.org.checkmycontrol.capture.Enum.AppVersion;
import lombok.Data;

/**
 * Created by zhi on 29/09/2015.
 */
@Data
public class AppUsage {
    private AppVersion appVersion;
    private Boolean alcoholConsumed;
}
