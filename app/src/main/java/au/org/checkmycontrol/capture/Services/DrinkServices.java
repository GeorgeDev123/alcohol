package au.org.checkmycontrol.capture.Services;

import android.content.Context;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.ArrayList;

import au.org.checkmycontrol.capture.Entities.Drink;
import au.org.checkmycontrol.capture.Enum.DrinkVolume;
import au.org.checkmycontrol.capture.Model.DrinkSize;
import au.org.checkmycontrol.capture.Model.DrinkSummary;
import au.org.checkmycontrol.capture.Model.DrinkType;

/**
 * Created by zhi on 3/10/2015.
 */
@EBean
public class DrinkServices {
    @RootContext
    Context mContext;

    public DrinkSummary createOrUpdateDrinkSummary(DrinkType drinkType, int size, int quantity){
        if(size < 0 || quantity < 1 || drinkType == null){
            return null;
        }
        DrinkSummary drinkSummary = new DrinkSummary();
        DrinkSize selectSize = drinkType.getSizes().get(size);
        drinkSummary.setQuantity(quantity);
        drinkSummary.setSize(selectSize.getDrinkSizeDisplay());
        drinkSummary.setSubmitEnum(drinkType.getSubmitEnum());
        drinkSummary.setSubmitSize(selectSize.getDrinkSizeSumbtionValue());
        drinkSummary.setImageSrc(drinkType.getImageId());
        drinkSummary.setDrinkType(drinkType);
        return drinkSummary;
    }

    public ArrayList<Drink> createDrink(ArrayList<DrinkSummary> selectedDrinks){
        ArrayList<Drink> drinks = new ArrayList<Drink>();
        for(DrinkSummary drinkSummary: selectedDrinks){
            Drink newDrink = new Drink();
            newDrink.setCount(Long.valueOf(drinkSummary.getQuantity()));
            newDrink.setType(au.org.checkmycontrol.capture.Enum.DrinkType.valueOf(drinkSummary.getSubmitEnum()));
            newDrink.setVolume(DrinkVolume.valueOf(drinkSummary.getSubmitSize()));
            drinks.add(newDrink);
        }
        return drinks;
    }
}
