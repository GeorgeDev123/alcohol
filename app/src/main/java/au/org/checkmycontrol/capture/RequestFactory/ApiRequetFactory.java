package au.org.checkmycontrol.capture.RequestFactory;

import android.os.Build;

import org.androidannotations.annotations.EBean;
import org.springframework.http.client.SimpleClientHttpRequestFactory;

import java.io.IOException;
import java.net.HttpURLConnection;

/**
 * Created by zhi on 22/10/2015.
 */
@EBean
public class ApiRequetFactory extends SimpleClientHttpRequestFactory {
    @Override
    protected void prepareConnection(HttpURLConnection connection, String httpMethod) throws IOException {
        super.prepareConnection(connection, httpMethod);
        if(Build.VERSION.SDK_INT == Build.VERSION_CODES.JELLY_BEAN) {
            setJellyBean();
        }
    }
    void setJellyBean(){
        System.setProperty("http.keepAlive", "false");
    }
}
