package au.org.checkmycontrol.capture.Services;

import org.androidannotations.annotations.rest.Accept;
import org.androidannotations.annotations.rest.Get;
import org.androidannotations.annotations.rest.Post;
import org.androidannotations.annotations.rest.RequiresAuthentication;
import org.androidannotations.annotations.rest.Rest;
import org.androidannotations.api.rest.MediaType;
import org.androidannotations.api.rest.RestClientErrorHandling;

import au.org.checkmycontrol.capture.Entities.AppUsage;
import au.org.checkmycontrol.capture.Entities.Participant;
import au.org.checkmycontrol.capture.Entities.Session;
import au.org.checkmycontrol.capture.MessageConverter.CustomMappingJackson2HttpMessageConverter;
import au.org.checkmycontrol.capture.RequestFactory.ApiRequetFactory;

/**
 * Created by zhizheng on 21/09/2015.
 */
@Rest(rootUrl = "http://115.146.94.138:8080/api", converters = {CustomMappingJackson2HttpMessageConverter.class }, requestFactory = ApiRequetFactory.class)
public interface IREST extends RestClientErrorHandling {

    @Get("/search/findByAppId?appId={appId}")
    @Accept(MediaType.APPLICATION_JSON)
    @RequiresAuthentication
    Object getAppId(String appId);

    @Get("/{id}")
    @Accept(MediaType.APPLICATION_JSON)
    Participant getParticipant(String id);

    @Post("/appdata/{appId}/appUsages")
    @Accept(MediaType.APPLICATION_JSON)
    @RequiresAuthentication
    Object saveAppUsage(String appId, AppUsage appUsage);

    @Post("/appdata/{appId}/sessions")
    @Accept(MediaType.APPLICATION_JSON)
    @RequiresAuthentication
    Object saveAppSession(String appId, Session session);

    @Get("/appdata/{appId}/appStatus")
    @Accept(MediaType.APPLICATION_JSON)
    @RequiresAuthentication
    Object checkAppStatus(String appId);

    void setHttpBasicAuth(String username, String password);
}
