package au.org.checkmycontrol.capture;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import au.org.checkmycontrol.capture.Model.DrinkType;
import au.org.checkmycontrol.capture.utils.DrinksTypesDataHolder;

public class ImageAdapter extends BaseAdapter {
    private Context mContext;
    private static LayoutInflater inflater;
    private ArrayList<DrinkType> drinks;

    // Constructor
    public ImageAdapter(Context c) {
        mContext = c;
        inflater = (LayoutInflater)mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
        drinks = DrinksTypesDataHolder.getInstance(mContext).getData();
    }

    public int getCount() {
        return drinks.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }
    public class Holder
    {
        TextView tv;
        ImageView img;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder=new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.item_drink_type, null);
        int screenHeight = ((Activity) mContext).getWindowManager()
                .getDefaultDisplay().getHeight();
        rowView.setMinimumHeight(screenHeight/3);

        holder.tv=(TextView) rowView.findViewById(R.id.tv_drink_type_name);
        holder.img=(ImageView) rowView.findViewById(R.id.img_drink_type_item);

        final DrinkType drink = drinks.get(position);
        holder.tv.setText(drink.getDrinkType());
        holder.img.setImageResource(drink.getImageId());
        rowView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, DrinkDetailsActivity_.class);
                intent.putExtra("Drink", drink);
                mContext.startActivity(intent);
            }
        });

        return rowView;
    }
}
