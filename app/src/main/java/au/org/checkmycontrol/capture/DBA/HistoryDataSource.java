package au.org.checkmycontrol.capture.DBA;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import org.androidannotations.annotations.EBean;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import au.org.checkmycontrol.capture.Model.SubmitionHistory;

/**
 * Created by zhi on 23/08/2015.
 */
@EBean
public class HistoryDataSource {
    private SQLiteDatabase database;
    private AlcoholSQLiteHelper dbHelper;
    private String[] allColumns = { AlcoholSQLiteHelper.COLUMN_ID,
            AlcoholSQLiteHelper.COLUMN_DATE };

    public HistoryDataSource(Context context) {
        dbHelper = new AlcoholSQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public SubmitionHistory createHistory(Date date) {
        ContentValues values = new ContentValues();
        values.put(AlcoholSQLiteHelper.COLUMN_DATE, getDateTime(date));
        long insertId = database.insert(AlcoholSQLiteHelper.TABLE_HISTORY, null,
                values);
        Cursor cursor = database.query(AlcoholSQLiteHelper.TABLE_HISTORY,
                allColumns, AlcoholSQLiteHelper.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        SubmitionHistory newComment = cursorToHistory(cursor);
        cursor.close();
        return newComment;
    }

    public ArrayList<SubmitionHistory> getLast24HourHistory(Date date) {
        ArrayList<SubmitionHistory> historys = new ArrayList<SubmitionHistory>();

        String queryString = "SELECT * FROM history WHERE date >= ?";
        String[] whereArgs = new String[]{getDateTime(date)};
//        Cursor cursor = database.query(AlcoholSQLiteHelper.TABLE_HISTORY,
//                allColumns, null, null, null, null, null);

        Cursor cursor = database.rawQuery(queryString,whereArgs);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            SubmitionHistory history = cursorToHistory(cursor);
            historys.add(history);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return historys;
    }

    private SubmitionHistory cursorToHistory(Cursor cursor) {
        SubmitionHistory history = new SubmitionHistory();
        history.setId(cursor.getLong(0));
        history.setDate(cursor.getString(1));
        return history;
    }

    private String getDateTime(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        return dateFormat.format(date);
    }
}
