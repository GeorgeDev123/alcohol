package au.org.checkmycontrol.capture.Exception;

import java.util.ArrayList;

import lombok.Getter;

/**
 * Created by zhi on 11/10/2015.
 */
public class ServerValidationException extends Exception {
    @Getter
    private ArrayList<String> messages;

    public ServerValidationException(String message){
        super(message);
    }
    public ServerValidationException(ArrayList<String> messages){
        this.messages = messages;
    }

}
